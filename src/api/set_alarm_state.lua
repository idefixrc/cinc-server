local logger = CinCLogger
local group_option_category = {}
local group = group
local AI = AI

group_option_category[2] = "Ground"

-- Example JSON
-- {
--   "name": "set_alarm_state",
--   "arguments": {
--     "group_name": "EXAMPLE_GROUP",
--     "alarm_state": "AUTO"
--   }
-- }
-- For a list of alarm states and their text names see https://wiki.hoggitworld.com/view/DCS_option_alarmState
local command = function(arguments)
  logger.info('Command set_alarm_state called')

  local group = Group.getByName(arguments['group_name'])
  local category_id = group:getCategory()
  local controller = group:getController()

  local state_id = AI['Option'][group_option_category[category_id]]['id']['ALARM_STATE']
  local state_value = AI['Option'][group_option_category[category_id]]['val']['ALARM_STATE'][arguments['alarm_state']]
  
  controller:setOption(state_id, state_value)

  local response = {}
  return response
end

CinCServer.add_command('set_alarm_state', command)