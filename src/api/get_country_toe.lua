local JSON = loadfile("Scripts\\JSON.lua")()
local io = io 
local logger = CinCLogger
local country = country
local type = type
local ipairs = ipairs
local table = table
local pcall = pcall

--- Get data about vehicle categories.
-- Gets data about vehicles (aka all ground units) and puts them into a table
-- We need this information to be able to properly categorise ground vehicles
-- since ground units in the Country table are all categorised as "cars"
-- @return a lua table keyed on the vehicle type
local function get_vehicle_data()
  local vehicle_data_by_type = {}
  local success, file = pcall(io.open, 'Scripts/cinc/data/vehicles.json', "r")
  if success then
    for _, entry in ipairs(JSON:decode(file:read("*a"))) do
      vehicle_data_by_type[entry['type']] = entry  
    end
  else
    logger.info("Error reading vehicles.json file:" .. file)
  end
  return vehicle_data_by_type
end

--- Get a country's TO&E (Table of Organization and Equipment)
-- This command should only ever be called once by a client once per mission
-- as this information does not change once a mission has started,
--
-- It returns the names of all the spawnable units for this country.
local command = function(arguments)
  logger.info('Command get_country_toe called')

  local response = {}

  if arguments['id'] == nil then
    response['Error'] = 'id parameter is required' 
  elseif type(arguments['id']) ~= "number" then
    response['Error'] = 'id parameter must be a number'
  elseif country['by_idx'][arguments['id']] == nil then
    response['Error'] = 'country with id ' .. arguments['id'] .. ' not found'
  else 
    local country = country['by_idx'][arguments['id']]  
    
    response['planes'] = {}
    response['helicopters'] = {}
    response['ships'] = {}
    response['heliports'] = {}
    response['artillery'] = {}
    response['armor'] = {}
    response['fortification'] = {}
    response['infantry'] = {}
    response['air_defence'] = {}
    response['unarmed'] = {}
    response['train'] = {}
      
    for _, plane in ipairs(country['Units']['Planes']['Plane']) do 
      table.insert(response['planes'], plane['Name'])
    end
    
    for _, helo in ipairs(country['Units']['Helicopters']['Helicopter']) do 
      table.insert(response['helicopters'], helo['Name'])
    end

    for _, ship in ipairs(country['Units']['Ships']['Ship']) do 
      table.insert(response['ships'], ship['Name'])
    end

    for _, heliport in ipairs(country['Units']['Heliports']['Heliport']) do 
      table.insert(response['heliports'], heliport['Name'])
    end
    
    -- Cars? CARS?? GROUND UNITS ARE ALL CARS! BUNKERS ARE CARS! TANKS ARE CARS! TRAINS? YOU GUESSED IT
    -- therefore we will assign them their category based on the json file created
    -- by scraping the vehicle data files.
    local vehicle_data_by_type = get_vehicle_data()
    for _, car in ipairs(country['Units']['Cars']['Car']) do 
      table.insert(response[vehicle_data_by_type[car['Name']]['category']:lower():gsub(" ", "_")], car['Name'])
    end
  end

  return response
  
end

CinCServer.add_command('get_country_toe', command)